package com.huanca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketappBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarketappBackendApplication.class, args);
	}
}
