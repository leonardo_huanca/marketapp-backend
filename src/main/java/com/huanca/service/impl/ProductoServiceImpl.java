package com.huanca.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huanca.dao.IProductoDAO;
import com.huanca.model.Producto;
import com.huanca.service.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private IProductoDAO dao;

	@Override
	public Producto registrar(Producto proucto) {
		return dao.save(proucto);
	}

	@Override
	public void modificar(Producto proucto) {
		dao.save(proucto);
	}

	@Override
	public void eliminar(int idProducto) {
		dao.delete(idProducto);
	}

	@Override
	public Producto listarId(int idProducto) {
		return dao.findOne(idProducto);
	}

	@Override
	public List<Producto> listar() {
		return dao.findAll();
	}

}
