package com.huanca.service;

import java.util.List;

import com.huanca.model.Producto;

public interface IProductoService {
	Producto registrar(Producto proucto);

	void modificar(Producto proucto);

	void eliminar(int idProducto);

	Producto listarId(int idProducto);

	List<Producto> listar();
}
